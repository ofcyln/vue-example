const fields = [
  {
    name: 'name',
    placeholder: 'Input name',
    type: 'text',
    component: 'Input'
  },
  {
    name: 'second',
    placeholder: 'Input second name',
    type: 'text',
    component: 'Input'
  },
  {
    name: 'address',
    placeholder: 'Input address',
    type: 'text',
    component: 'Input'
  },
  {
    name: 'phone',
    placeholder: 'Input your phone',
    type: 'number',
    component: 'Input'
  },
  {
    name: 'about',
    placeholder: 'Tell me your story',
    component: 'TextArea'
  }
]
 
export default fields
