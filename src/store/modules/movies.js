import axios from 'axios'

const actions = {
  async fetchBeers({ commit }) {
    const { data } = await axios.get('https://api.punkapi.com/v2/beers')
    await commit('setBeers', { data })
  }
}

const mutations = {
  setBeers(_, { data }) {
    state.beers = data
  }
}

const getters = {
  beersArray: () => state.beers
}

const state = {
  beers: []
}

export default {
  actions,
  mutations,
  getters,
  state
}
