import Vuex from 'vuex'
import Vue from 'vue'
import movies from './modules/movies'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    movies
  }
})

export default store